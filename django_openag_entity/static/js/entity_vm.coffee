root = exports ? this
root.entity = root.entity ? {}

class Tag
  constructor:(id, value)->
    @id = id
    @value = value

class EntitySelectViewModel
  constructor:->
    @init = false
    @idToTag = {}
    @selectedTags = ko.observableArray()
    @availableTags = ko.observableArray()
    @globalTags = ko.observableArray()
    @selectedSelected = ko.observableArray()
    @selectedAvailable = ko.observableArray()
    @select = ()=>
      for id in @selectedAvailable()
        @addTag(@idToTag[id])
      @selectedAvailable.removeAll()

    @remove = ()=>
      for id in @selectedSelected()
        @removeTag(@idToTag[id])
      @selectedSelected.removeAll()

    @addTag = (tag)=>
      @availableTags.remove(tag)
      @selectedTags.push(tag)

    @removeTag = (tag)=>
      @selectedTags.remove(tag)
      @availableTags.push(tag)

    @_init_add_tag= (id, value)=>
      if @init
        return
      tag = new Tag(id, value)
      @idToTag[id] = tag
      @availableTags.push(tag)

    @_init_complete = =>
      @init = true

root.entity.EntitySelectViewModel = EntitySelectViewModel

