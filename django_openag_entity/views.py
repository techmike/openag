from django.shortcuts import render, render_to_response
from django.template import Context, loader
from core import models as CoreModels

# Create your views here.

def entity_select(request):
    user = request.user
    tags = CoreModels.Tag.objects(user=user)
    context = {
        'request': request,
        'tags': tags,
    }
    t = loader.get_template('entity/entity-select.html')
    return t.render(context)
