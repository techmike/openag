from .views import entity_select

class EntitySelectWidget:
    js_viewmodel_script = '/static/js/entity_vm.js'
    ko_viewmodel = 'entity.EntitySelectViewModel'
    ko_with_key = 'entitySelector'

    def render(self, request):
        return entity_select(request)
