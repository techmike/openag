from django.conf.urls import patterns, include, url
from django.contrib import admin
from core import views as CoreViews
from core import urls as CoreURLs
admin.autodiscover()



urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'openag.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += CoreURLs.urls

# Must be added at the end after all patterns added.
urlpatterns += [
    url(r'^api/', include(CoreURLs.router.urls), name='api'),
    url(r'^api/log_message', CoreViews.post_log_messages_json, name='log_message'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]