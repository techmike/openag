function JsonAppender(url) {
    var isSupported = true;
    var successCallback = function(data, textStatus, jqXHR) { return; };
    if (!url) {
        isSupported = false;
    }
    this.setSuccessCallback = function(successCallbackParam) {
        successCallback = successCallbackParam;
    };
    this.append = function (loggingEvent) {
        if (!isSupported) {
            return;
        }
        csrftoken = $.cookie('csrftoken');
        $.ajaxSetup({
          beforeSend: function(xhr, settings){xhr.setRequestHeader("X-CSRFToken", csrftoken)}
        });
        $.post(url, {
            'logger': loggingEvent.logger.name,
            'timestamp': loggingEvent.timeStampInMilliseconds,
            'level': loggingEvent.level.name,
            'url': window.location.href,
            'message': loggingEvent.getCombinedMessages(),
            'exception': loggingEvent.getThrowableStrRep()
        }, successCallback, 'json');
    };
}

JsonAppender.prototype = new log4javascript.Appender();
JsonAppender.prototype.toString = function() {
    return 'JsonAppender';
};
log4javascript.JsonAppender = JsonAppender;