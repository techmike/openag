root = exports ? this
root.farm_data = root.farm_data ? {}


class SelectableMixin

  constructor:(self)->
    self.selected = ko.observable(false)
    self.toggleSelected = ()=>
      if self.selected()
        self.selected(false)
      else
        self.selected(true)
root.farm_data.SelectableMixin = SelectableMixin



class ValidationMixin

  constructor:(self)->
    self.validator = self.validator ? {}

    self.is_valid = (property=null)=>
      isValid = false
      if property
        func = self.validator[property]
        isValid = func()
      else
        for p in self.validator
          func = self.validator[property]
          isValid = func()
          if not isValid
            break
      return isValid

    self.isNotEmptyString = (string)=>
      return string != null && string != undefined && $.trim(string) != ''
root.farm_data.ValidationMixin = ValidationMixin