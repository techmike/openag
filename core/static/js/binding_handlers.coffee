GLYPHICON_OK = 'glyphicon-ok'
GLYPHICON_REMOVE = 'glyphicon-remove'
HAS_SUCCESS = 'has-success'
HAS_ERROR = 'has-error'
DANGER_BG = 'danger-bg'
SUCCESS_BG = 'success-bg'
VALIDATION_FAILURE = 'validation-failure'


ko.bindingHandlers.filestyle = {
  init: (element, valueAccessor, allBindings, viewModel, bindingContext)=>
    try
      logger.debug('filestyle', 'Setting up filestyle')
      div = $(element)
      data = valueAccessor()
      div.append('<input type="file" class="filestyle" data-input="false">')
      button = div.children().first()
      id = data['id']
      logger.debug('filestyle_id', '' + id)
      button.filestyle({id:id, input: false});
      button.change(()->
        reader = new FileReader()
        file = this.files[0];
        name = file.name
        reader.onloadend = (e)=>
          url = e.target.result
          viewModel.filename(name)
          viewModel.url(url)
        reader.readAsDataURL(file)
      )
    catch error
      logger.error('filestyle', 'Error occurred while uploading file.')
      logger.error('filestyle', error)
}


ko.bindingHandlers.file = {
  init: (element, valueAccessor, allBindings) =>
    fileContents = null
    fileName = null
    allowed = null
    prohibited = null
    reader = null
    index = 0
    data = valueAccessor()
    if ((typeof data) == "function")
      fileContents = data;
    else
      fileContents = $(data['data']);
      fileName = $(data['name']);
      if data['index']
        index = data['index']

      allowed = data['allowed'];
      if ((typeof allowed) == 'string')
        allowed = [allowed];

      prohibited = data['prohibited'];
      if ((typeof prohibited) == 'string')
        prohibited = [prohibited];
      reader = (data['reader']);

    handler = (fileC) =>
      file = element.files[index];
      # Opening the file picker then canceling will trigger a 'change'
      # event without actually picking a file.
      if (file == undefined)
        fileC(null)
        return;

      if (allowed)
        if (!allowed.some((type)=>  return type == file.type))
          fileC(null)
          return;

      if (prohibited)
        if (prohibited.some((type) => return type == file.type))
          fileC(null)
          return;
      reader = new FileReader()
      reader.onloadend = (e)=>
        url = e.target.result
        fileC(url)
      reader.readAsDataURL(file);
      if (typeof fileName == "function")
        fileName.val(file.name)
    $(element).change(()=>handler(fileContents))
  }


ko.bindingHandlers.ignoreClick = {
  init:(element)=>
    $(element).click(()=>event.stopPropagation())
}


ko.bindingHandlers.validationBox = {
  init: (element, valueAccessor, allBindings, viewModel, bindingContext)=>

  update: (element, valueAccessor, allBindings, viewModel, bindingContext)=>
    data = valueAccessor()
    key = data['key']
    box = $(element)
    logger.debug('validationBox', 'Validating Element: ' + $(element).attr('id'))
    isValid = false
    try
      isValid = viewModel.is_valid(key)
    catch error
      logger.error('validationBox', 'Error validating. Error: ' + error)
    if isValid
      box.find('span').addClass(GLYPHICON_OK).removeClass(GLYPHICON_REMOVE)
      box.addClass(SUCCESS_BG).removeClass(DANGER_BG)
      box.removeClass(VALIDATION_FAILURE)
    else
      box.find('span').addClass(GLYPHICON_REMOVE).removeClass(GLYPHICON_OK)
      box.addClass(DANGER_BG).removeClass(SUCCESS_BG)
      box.addClass(VALIDATION_FAILURE)
}


ko.bindingHandlers.selectableRow = {
  init: (element, valueAccessor, allBindings, viewModel, bindingContext)=>
    row = $(element)
    row.click(
      ()=>
        viewModel.toggleSelected()
    )

  update: (element, valueAccessor, allBindings, viewModel, bindingContext)=>
    row = $(element)
    if viewModel.selected()
      row.addClass('row-selected')
    else
      row.removeClass('row-selected')

}


ko.bindingHandlers.dateButton = {
    init: (element, valueAccessor, allBindings, viewModel, bindingContext)=>
      button = $(element);
      button.click(()=>
        event.stopPropagation()
        settings = valueAccessor()
        dateButton = null
        if settings['min']
          dateButton = DateButton.create_limit_date_before_today(button);
        else if settings['max']
          dateButton = DateButton.create_limit_date_after_today(button);
        else
          dateButton = new DateButton(button)
        dateButton.value_set_callback = (value)=>
          viewModel.date(value)
        dateButton.open()
      )
};


ko.bindingHandlers.timeButton = {
    init: (element, valueAccessor, allBindings, viewModel, bindingContext)=>
      button = $(element);
      button.click(()=>
        event.stopPropagation()
        timeButton = new TimeButton(button);
        timeButton.value_set_callback = (value)=>
          viewModel.time(value)
        timeButton.open()
      )
};

ko.bindingHandlers.dialog = {
  init: (button, valueAccessor, allBindings, viewModel, bindingContext)=>
    settings = valueAccessor()
    popupElement = $(settings['dialog'])
    callback = settings['callback']
    data = {
      resizable: true,
      height: 200,
      modal: true,
      buttons: {
        Ok: ->
          $(this).dialog("close");
          if callback
            viewModel[callback](true)
        Cancel: ->
          $(this).dialog("close");
          if callback
            viewModel[callback](false)
      }
    }
    #for key in settings
    #  data[key] = settings[key]
    $(button).click(()=>
      popupElement.dialog(data).dialog('open')
    )
}
