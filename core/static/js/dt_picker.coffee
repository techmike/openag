class BaseDTButton

  constructor:(inputButton)->
    @inputButton = inputButton

  value_set_callback: (value) => None

  send_callback:(value)=>
    if @value_set_callback
      @value_set_callback(value)

  setup_picker:(picker)=>
    @inputButton.click(( event ) ->
      if ( picker.get( 'open' ) )
        picker.close();
      else
        picker.open();
      event.stopPropagation();
    );

class DateButton extends BaseDTButton

  constructor:(inputButton, settings={})->
    super(inputButton)
    @min = settings['min']
    @max = settings['max']

  @create_limit_date_before_today =(inputButton)->
    m = moment()
    todaysDate = new Date(m.get('year'), m.get("month"), m.date())
    settings = {'min':todaysDate}
    return new DateButton(inputButton, settings)

  @create_limit_date_after_today =(inputButton)->
    m = moment()
    todaysDate = new Date(m.get('year'), m.get("month"), m.date())
    settings = {'max':todaysDate}
    return new DateButton(inputButton, settings)


  open:()=>
    m = moment()
    todaysDate = new Date(m.get('year'), m.get("month"), m.date())
    @inputButton.pickadate({
      format: 'mm/dd/yyyy'
      formatSubmit: 'mm/dd/yyyy'
      hiddenName: true
      min: @min
      max: @max
      onClose: () =>
        picker = @inputButton.pickadate('picker')
        @send_callback(picker.get())
    })
    picker = @inputButton.pickadate('picker')
    @setup_picker(picker)
    @inputButton.pickadate('picker').open()


class TimeButton extends BaseDTButton

  open:()=>
    @inputButton.pickatime({
      format: 'H:i'
      formatSubmit: 'H:i'
      hiddenName: true
      onClose: () =>
        picker = @inputButton.pickatime('picker')
        @send_callback(picker.get())
    })
    picker = @inputButton.pickatime('picker')
    @setup_picker(picker)
    @inputButton.pickatime('picker').open()


window.DateButton = DateButton
window.TimeButton = TimeButton