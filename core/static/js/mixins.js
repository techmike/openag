// Generated by CoffeeScript 1.9.1
(function() {
  var SelectableMixin, ValidationMixin, ref, root;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  root.farm_data = (ref = root.farm_data) != null ? ref : {};

  SelectableMixin = (function() {
    function SelectableMixin(self) {
      self.selected = ko.observable(false);
      self.toggleSelected = (function(_this) {
        return function() {
          if (self.selected()) {
            return self.selected(false);
          } else {
            return self.selected(true);
          }
        };
      })(this);
    }

    return SelectableMixin;

  })();

  root.farm_data.SelectableMixin = SelectableMixin;

  ValidationMixin = (function() {
    function ValidationMixin(self) {
      var ref1;
      self.validator = (ref1 = self.validator) != null ? ref1 : {};
      self.is_valid = (function(_this) {
        return function(property) {
          var func, i, isValid, len, p, ref2;
          if (property == null) {
            property = null;
          }
          isValid = false;
          if (property) {
            func = self.validator[property];
            isValid = func();
          } else {
            ref2 = self.validator;
            for (i = 0, len = ref2.length; i < len; i++) {
              p = ref2[i];
              func = self.validator[property];
              isValid = func();
              if (!isValid) {
                break;
              }
            }
          }
          return isValid;
        };
      })(this);
      self.isNotEmptyString = (function(_this) {
        return function(string) {
          return string !== null && string !== void 0 && $.trim(string) !== '';
        };
      })(this);
    }

    return ValidationMixin;

  })();

  root.farm_data.ValidationMixin = ValidationMixin;

}).call(this);

//# sourceMappingURL=mixins.js.map
