root = exports ? this

class AppLogger
  _appenders = []
  _loggerToAppended = {}
  _haveAppendedDebug = false

  _init = ()=>
    jsonAppender = new log4javascript.JsonAppender("/api/log_message", true);
    jsonAppender.setThreshold(log4javascript.Level.ERROR);
    _appenders.push(jsonAppender)
  _init()

  _get_logger = (name)=>
    logger = log4javascript.getLogger(name);
    if not _loggerToAppended[logger]
      _.each(_appenders, (appender)=>
        logger.addAppender(appender)
      )
      _loggerToAppended[logger] = true
    return logger


  constructor:(debug=false)->
    if debug and not _haveAppendedDebug
      #console = new log4javascript.BrowserConsoleAppender()
      #console.setLayout(new log4javascript.JsonLayout(true))
      #_appenders.push(console)
      popup = new log4javascript.PopUpAppender()
      popup.setLayout(new log4javascript.JsonLayout(true))
      _appenders.push(popup)
      _haveAppendedDebug = true

  info:(name, message)->
    logger = _get_logger(name)
    logger.info(message)

  debug:(name, message)->
    logger = _get_logger(name)
    logger.debug(message)

  warn:(name, message)->
    logger = _get_logger(name)
    logger.warn(message)

  error:(name, message)->
    logger = _get_logger(name)
    logger.error(message)





root.AppLogger = AppLogger
