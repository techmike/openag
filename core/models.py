from mongoengine.django.auth import User
from django.contrib.auth.models import AnonymousUser
from mongoengine import *

import datetime

try:
    from django.utils.module_loading import import_module
except ImportError:
    """Handle older versions of Django"""
    from django.utils.importlib import import_module

"""
CREATE AN EVENT CLASS. The entire action/observation is an event.
The event will tie together the entities. The entities exist in
the spaces, and they're connected via relationships (tags) and
events that occur to them (by the user) or happen and are noticed
(by the user).

CREATE A TAG CLASS.
The tag class MUST be implemented. The "tag" should most likely be
the actual entity itself. The tag will have metadata about it
documentation, etc that is posted by the users. One could consider it
similar to a wikipedia page. Each tag, is connected to other tags,
via being tagged. The relationship is actually its own tag. So the
connection between boer goat and goat is made BY the tag.

Example:
[Boer Goat.] [ Is a type of ] [Goat.]
These are three tags. What tags boer to goat is the "is a type of" tag.
How exactly these tags work and is maintained isn't a big deal,
however they are the glue that ties together the entities. Optimizations
can be built in later if this ever becomes difficult to navigate, however,
navigating and building bubbling or tunneling capabilities is possible.


"""



"""
Requirements:

[Propagated/linked observations]

Details:
If you observe a whole you the observation INCLUDES the parts.

I want to propagate by associated relationships. So if I observe
something about goats, I should be able to look at all observations of
of "seeker" and still see the observations on goats.

If I perform an action on the front yard

"""


class DTMixin:
    datetime = DateTimeField(required=True, default=datetime.datetime.now)


class DatedDocument(Document):
    created = DateTimeField(default=datetime.datetime.now)
    updated = DateTimeField(default=datetime.datetime.now)
    meta = {'allow_inheritance': True,
            'abstract': True,
            }


    def save(self, force_insert=False, validate=True, clean=True,
             write_concern=None,  cascade=None, cascade_kwargs=None,
             _refs=None, save_condition=None, **kwargs):
        self.updated = datetime.datetime.now
        super().save(self)


class UserGeneratedContent(Document):
    user = ReferenceField(User, required=True)
    meta = {'allow_inheritance': True,
            'abstract': True,
            }

    def save(self, force_insert=False, validate=True, clean=True,
             write_concern=None,  cascade=None, cascade_kwargs=None,
             _refs=None, save_condition=None, **kwargs):
        if not self.user or not self.user.username:
            raise Exception("User must not be null.")
        super().save(self)

class WidgetDescription(DatedDocument):
    user = ReferenceField(User, required=True)
    name = StringField(required=True)
    widget_path =StringField(required=True)

    @staticmethod
    def get_or_create(user, name, widgetPath):
        obj = WidgetDescription.objects(user=user)(name=name)(widget_path=widgetPath)
        if not obj:
            obj = WidgetDescription(user=user, name=name, widget_path=widgetPath)
            print(obj.user)
            print(obj.name)
            print(obj.widget_path)
            obj.save()
        return obj

    @staticmethod
    def get_widgets(user):
        if isinstance(user,AnonymousUser):
            return []
        descriptions = WidgetDescription.objects(user=user)
        widgets = []
        for desc in descriptions:
            dot = desc.widget_path.rindex('.')
            module = import_module(desc.widget_path[:dot])
            widget = getattr(module, desc.widget_path[dot + 1:])
            widgets.append(widget)
        return widgets


class Measurement(EmbeddedDocument):
    UNITS = (
        ('na', 'N/A'),
        ('in', 'Inches'),
        ('ft', 'Feet'),
        ('lb', 'Pounds'),
        ('oz', 'Ounces'),
        ('ga', 'Gallons'),
        ('me', 'Meters'),
        ('gr', 'Grams'),
        ('ki', 'Kilograms'),
    )
    unit = StringField(choices=UNITS)
    value = DecimalField()


class Documentation(DatedDocument, UserGeneratedContent, DTMixin):
    """
    A class that documents information about an entity.
    It provides a single piece of documentation about an entity
    and is dated.
    """
    measurement = EmbeddedDocumentField(Measurement)
    details = StringField()
    picture = ImageField()


class DocumentationMixin:
    documentation = ListField(ReferenceField(Documentation))


class Tag(DatedDocument, UserGeneratedContent, DocumentationMixin):
    """
    A tag is everything, its tag and "context" is what
    makes it unique.
    It has tags on it from other tags.
    Each tag is time stamped. It has when it was first created
    and when ever it is updated it has that time updated.
    """
    value = StringField(required=True)
    is_abstract = BooleanField()
    # Only limitation is it can not tag itself. For obvious reasons.
    tags = ListField(ReferenceField('Tag'))
    categories = ListField(ReferenceField('Tag'))

    def get_geo_history(self):
        return GeoHistory.objects(tag=self).first()


class Notification(UserGeneratedContent):
    # The tags related to this notification
    tags = ListField(ReferenceField(Tag))
    notify_date = DateTimeField(required=True, verbose_name='Notify Date')
    title = StringField(required=True)
    details = StringField()


class GeoTag(EmbeddedDocument, DTMixin):
    """
    A single geographic location
    If the location is a named place the tag should be used.
    Otherwise the lat/lon should be used.
    """
    # The location as a tag, if its a named place
    location = ReferenceField(Tag)
    lat = DecimalField()
    lon = DecimalField()


class GeoHistory(UserGeneratedContent):
    """
    All of the history of an object as far as its movements and location.
    """
    # Only one history per tag.
    tag = ReferenceField(Tag, required=True, unique=True)
    history = EmbeddedDocumentListField(GeoTag)


class Event(DatedDocument, DTMixin, UserGeneratedContent, DocumentationMixin):
    """
    A thing that occurs that is captured via the system.
    The user captures some sort of event that occurs. The user
    notices something, the user did something, this thing occurred,
    and it needs to be written down.
    """
    value = StringField(required=True)


class Analysis(DatedDocument, DTMixin, UserGeneratedContent, DocumentationMixin):
    """
    A belief or statement about a tag/event that says some sort of thing
    in the future. Sourcing on this will be important.
    """
    event = ReferenceField(Event)
    details = StringField()


class LogMessage(Document):
    LOG_CHOICES = (
        ('er', 'ERROR'),
        ('wa', 'WARN'),
    )
    CHOICE_TO_LEVEL = {
        'ERROR': 'er',
        'WARN': 'wa',
    }
    logger = StringField()
    timestamp = DateTimeField()
    level = StringField(max_length=2, choices=LOG_CHOICES)
    message = StringField()
    exception = StringField()
