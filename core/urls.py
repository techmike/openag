from django.conf.urls import include, url
from rest_framework import routers
from core import views as CoreViews

router = routers.DefaultRouter()
#router.register(r'user', CoreViews.UserViewSet)

urls = [
    url(r'^$', CoreViews.HomeView.as_view(), name='home'),
]


# Add URLS
urls += [
    #url(r'^add/observation/$', CoreViews.add_observation, name='add_observation'),
    #url(r'^add/action/$', CoreViews.add_action, name='add_action'),
    #url(r'^add/entity/$', CoreViews.add_entity, name='add_entity'),
]

# API URLS
urls += [
    #url(r'^api/position/with_entities/(?P<siteID>\d+)$', CoreViews.get_positions_from_site_with_entities,
    #    name='position_with_entities'),
]