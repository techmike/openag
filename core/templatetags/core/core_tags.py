from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.html import escape
from openag import settings
from core import models as CoreModels

register = template.Library()


@register.simple_tag
def render_widget(request, widget):
    return widget.render(request)




@register.simple_tag
def to_javascript_bool(pyBool):
    s = str(pyBool)
    s = s.lower()
    if not s:
        return 'false'
    return s


@register.simple_tag
def JSON_DEBUG():
    return str(settings.DEBUG).lower()

