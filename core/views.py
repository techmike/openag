from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import JsonResponse, HttpResponseBadRequest
from core import models as CoreModels
from core.models import LogMessage
from datetime import datetime

# Create your views here.
def post_log_messages_json(request):
    if request.POST:
        data = request.POST
        message = LogMessage()
        message.logger = data['logger']
        message.level = LogMessage.CHOICE_TO_LEVEL[data['level']]
        message.message = data['message']
        message.exception = data['exception']
        timestamp = data['timestamp']
        message.timestamp = datetime.fromtimestamp(int(timestamp[:10]))
        message.save()
        return JsonResponse({'response':'ok'})
    return HttpResponseBadRequest()

class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['widgets'] = CoreModels.WidgetDescription.get_widgets(self.request.user)
        return context

